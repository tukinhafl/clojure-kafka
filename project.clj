(defproject producer-consumer "0.1.0"
  :description "Create a kafka producer and high level consumer"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.10.3"]
                 [org.apache.kafka/kafka-clients "3.0.0"]
                 [org.apache.kafka/kafka_2.12 "3.0.0"]]
  :aot [producer-consumer.core]
  :main producer-consumer.core)
