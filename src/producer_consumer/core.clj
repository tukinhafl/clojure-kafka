(ns producer-consumer.core
  (:gen-class)
  (:import [org.apache.kafka.clients.admin AdminClientConfig NewTopic KafkaAdminClient]
           org.apache.kafka.clients.consumer.KafkaConsumer
           [org.apache.kafka.clients.producer KafkaProducer ProducerRecord]
           [org.apache.kafka.common.serialization StringDeserializer StringSerializer]))

(defn create-topics!
  "Create the topic "
  [bootstrap-server topics ^Integer partitions ^Short replication]
  (let [config {AdminClientConfig/BOOTSTRAP_SERVERS_CONFIG bootstrap-server}
        adminClient (KafkaAdminClient/create config)
        new-topics (map (fn [^String topic-name] (NewTopic. topic-name partitions replication)) topics)]
    (.createTopics adminClient new-topics)))

(defn build-consumer
  "Create the consumer instance to consume
from the provided kafka topic name"
  [bootstrap-server]
  (let [consumer-props
        {"bootstrap.servers",  bootstrap-server
         "group.id",           "example"
         "key.deserializer",   StringDeserializer
         "value.deserializer", StringDeserializer
         "auto.offset.reset",  "earliest"
         "enable.auto.commit", "true"}]
    (KafkaConsumer. consumer-props)))

(defn consumer-subscribe
  [consumer topic]
  (.subscribe consumer [topic]))

(defn build-producer ^KafkaProducer
  [bootstrap-server]
  (let [producer-props {"value.serializer"  StringSerializer
                        "key.serializer"    StringSerializer
                        "bootstrap.servers" bootstrap-server}]
    (KafkaProducer. producer-props)))

(defn run-application
  "Create the simple read and write topology with Kafka"
  [bootstrap-server]
  (let [consumer-topic "example-consumer-topic"
        producer-topic "example-produced-topic"
        consumer (build-consumer bootstrap-server)
        producer (build-producer bootstrap-server)]
    (println "Creating the topics %s" [producer-topic consumer-topic])
    (create-topics! bootstrap-server [producer-topic consumer-topic] 1 1)
    (println "Starting the kafka example app. With topic consuming topic %s and producing to %s"
               consumer-topic producer-topic)
    (consumer-subscribe consumer consumer-topic)
    
    (loop []
      (let [num (str (rand-int 1000))]
        (println (str "Sending to Kafka topic " producer-topic ": " num))
        (.send producer (ProducerRecord. producer-topic (str "Processed Value: " num)))
        (Thread/sleep 1500)
        (recur)))))

(defn -main
  []
  (.addShutdownHook (Runtime/getRuntime) (Thread. #(println "Shutting down")))
  (run-application "localhost:9092"))